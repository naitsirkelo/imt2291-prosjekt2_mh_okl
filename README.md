# StudentTube 2.0
## IMT2291 Prosjekt 2 (V2019)

![Build Status](https://img.shields.io/badge/build-passing-green.svg)


## Prosjektdeltakere

- Marius Håkonsen
- Ole Larsen


### Bakgrunn
"Dette prosjektet er en fortsettelse på prosjekt 1 i den forstand at dere skal jobbe videre med et nettsted for undervisningsvideoer.
I motsetning til prosjekt 1 hvor fokuset var på serversideteknologi vil nå fokuset være på klientsiden.
Applikasjonen skal denne gangen utvikles som en single page application ved hjelp av Polymer (https://www.polymer-project.org/)
Mye av koden dere skrev for prosjekt 1 kan brukes opp igjen i dette prosjektet men det blir også en god del ny kode også på serversiden."

Link til repo dette prosjektet er forket fra: https://bitbucket.org/okolloen/imt2291-prosjekt2-2019/src/master/  
Original prosjekt wiki: https://bitbucket.org/okolloen/imt2291-prosjekt2-2019/wiki/Home  


## Rapport

- Koden er skrevet på engelsk for at den enklere kan tilpasses syntaxen, mens displayet i applikasjonen er hovedsaklig på norsk.


### Starte prosjektet

- Klon og last ned repository fra Bitbucket.  

Manuell installering:  
- Åpne ReadMe i 'prosjekt2-work'  
- Følg oppskrift for å sette opp Docker-miljøet.  

Automatisk installasjon:  
- Naviger deg til hovedmappen 'imt2291-prosjekt2_mh_okl'  
- Kjør bash-scriptet 'setup.bash' etterfulgt av 'restart.bash'.  
- Sjekk installasjon ved å kjøre 'test.bash'.  

Vær oppmerksom på at det kan ta litt tid før docker har satt opp miljøet og localhost har lastet alle elementene.  


### Tilganger

Mappene uploads/ og thumbnails/ må ha skrive og leserettigheter.
Hvis du får feilmelding om at du ikke har tilgang, naviger til disse to mappene og skriv inn kommandoene **sudo chmod 777 uploads** og **sudo chmod 777 thumbnails**.  


### Testing

For å kjøre Unit testene i prosjektet kjører du følgende kommando fra root directory i prosjektet: ./runTests.bash  
Hvis du får feilmelding om at filen ikke er eksekverbar, skriv kommandoen: chmod +x runTests.bash  


### Verktøy

* php  
* HTML  
* JavaScript  
* Bitbucket (Filen ved navn 'push.bash' benyttes kun til oppdatering av repoet.)  


### Databasen

Følgende lister viser de ulike tablene som ligger i databasen, og med beskrivelse av kolonnene dataen skal lagres i.  

- user table:  
    - username: Email, kun gyldig ifølge html pattern, lagret som text.
    - password: Hashet.
    - privilegies: 0 = Student, 1 = Teacher, 2 = Administrator.
    - approved: 0 = Foreløpig ikke godkjent Lærer. 1 = Godkjent lærer (og administratorer) med lærerprivilegier.
    - rejected: 0 = Standard verdi. 1 = Avviste lærerbrukere.

- uploads table:  
    - id: auto increment felt for å enklere henvise til spesifikke videoer
    - owner: Læreren som lastet opp videoen.
    - fileName: Navnen på filen og formatet.
    - description: Kort beskrivelse av videoen.
    - filePath: Hvor videoen ligger lagret lokalt.
    - thumbnail: (Valgfritt) Path til egendefinert bilde for å beskrive videoen.
    - title: Videoens egendefinerte tittel.
    - avgRating: Float basert på alle vurderinger gitt av studenter.
    - totalScore: Int som holder styr på total score for å sammen med NoOfVotes kunne regne ut gjennomsnitt.
    -NoOfVotes: Int som viser hvor mange som har stemt på videoen.
    - playlistParent: Navnet på spillelisten videoen ligger under. NULL hvis ingen spilleliste.

- ratings table:  
    - username: Eleven som ga vurderingen - kun én per video.
    - videoID: Tittel på videoen som ble vurdert.
    - rating: Vurderingen - 1 til 5 (integer).

- comments table:  
    - username: Eleven som ga vurderingen - kun én per video.
    - videoID: Id på videoen som ble vurdert.
    - comment: Kommentar - tekst.

- playlists - Vurderinger av videoer lagres i databasen med følgene data:  
    - id: auto increment felt for å enklere henvise til spesifikke spillelister
    - title: Navnet på spillelisten, laget av en lærer.
    - description: Kort beskrivelse av spillelisten.
    - owner: Læreren som opprettet spillelisten.
    - subject: Emner ved skolen, f.eks IMT2291.
    - theme: Hva slags videoer skal ligge i denne listen.
    - thumbnail: Path til egendefinert bilde for å beskrive spillelisten.

- subscriptions - Vurderinger av videoer lagres i databasen med følgene data:  
    - student: Brukernavn på elev som opprett et abonnement på spillelisten.
    - playlist: Id til den abonnerte spillelisten.


### Brukere

Følgende brukere er lagt inn i databasen for testingens del:  
- username: **teacher1@a.no**, password: **laerer** (Lærer med opplastede videoer)  
- username: **student**, password: **student** (Eksempel studentbruker)  
- username: **admin**, password: **admin** (Eksempel administratorbruker)  

### API

**approveteacher.php**  
- Gets ID from manage-user-view.  
- Update selected ID to give new user teacher privileges.  
- Returning result status.  

**avatar.php**  
- Called in homepage to replace the general person icon.  
- Try to get path to custom profile picture of user.  
- Return path only if found.  

**deleteplaylist.php**  
- Get chosen playlist ID and thumbnail, from manage-playlists-view.  
- Store result as a row count in a custom data array.  
- Deleting local storage at thumbnail path.  
- Returns the updated row count to manage view.  

**deleteuser.php**  
- Get ID and avatar path of chosen user.  
- Try to delete user from database.  
- Delete locally stored user profile picture.  
- Return custom data array with row count.  

**deletevideo.php**  
- Get chosen video ID and path, and path to thumbnail if set.  
- Delete locally stored video and, if it exists, thumbnail.  
- Return custom data array with row count.  

**getplaylistvideos.php**  
- Get chosen playlist ID from playlists-view.
- Selecting all info about all videos currently uploaded to database.
- Returning array of videos found with fetchAll to videos-view.

**getsingleplaylist.php**  
- Get chosen playlist ID in playlist-view.
- Returning all info about playlist to the playlist-view.

**getsingleuser.php**  
- Get chosen user ID from admin-view.  
- Returns user id, username, type and avatar in manage-user-view.  

**getsinglevideo.php**  
- Get chosen video ID in videos-view.  
- Returning all info about the video to either subtitles-view or upload-thumbnail-view.  

**getusers.php**  
- Getting user id, username, type and avatar (default = null) of all users.  
- Executing statement to array.  
- Returning as fetchAll to show all users in admin-view.  

**login.php (from polymer2019 example)**  
- Trying to log in username and password from input fields.  
- Matching username to database and then verifies password if user is found.  
- Returning result array with user data if successful, and an updated status message.  

**makeuseradmin.php**  
- Gets ID from manage-user-view.  
- Update selected ID to give new user teacher privileges.  
- Returning result status.  

**myplaylists.php**  
- Gets current user ID (- only available for teachers).  
- Finds current users username.  
- Gets playlists with the user set as owner.  
- Returns result with fetchAll to manage-playlists-view.  

**myvideos.php**  
- Gets current user ID (- only available for teachers).  
- Finds current users username.  
- Gets videos with the user set as owner.  
- Returns result with fetchAll to manage-playlists-view.  

**newplaylist.php**  
- Gets current user ID (- only available for teachers).  
- Finds current users username.  
- Getting chosen variables from manage-playlists-view.  
- Storing chosen file in local thumbnail directory.  
- Insert new row with playlist into database.  
- Returning data array with thumbnail path and status to manage-playlists-view.  

**playlists.php**  
- Getting info about all playlists from database.  
- Returning array with fetchAll to playlists-view.  

**ratevideo.php**    
- Get the videoid and rating given.  
- Finds current users username.  
- Insert rating into rating table.  
- Get the totalScore and noOfVotes from db for video with id.  
- Add 1 to noOfVotes.  
- Add rating to total score.  
- Calculate average rating.  
- Returning custom data with status.  

**removeprivileges.php**  
- Get chosen user ID.  
- Update selected ID to give the user only student privileges.  
- Returning result status to manage-user-view.  

**searchplaylist.php**  
- Store criteria variable from the search input field on the homepage playlist-view.  
- Get playlist item from database with any column matching criteria of search.  
- Returning data array with found playlists to an updated playlists-view.  

**searchvideo.php**  
- Store criteria variable from the search input field on the homepage videos-view.  
- Get playlist item from database with any column matching criteria of search.  
- Returning data array with found videos to an updated videos-view.  

**subscribe.php**  
- Get chosen user and playlist ID.  
- Try to get the already stored subscription from database. If it exists, return error message.  
- Insert new subscription, if not already existing.  
- Returning result status to playlists-view.  

**subscribedplaylists.php**  
- Gets current student user ID.  
- Gets playlists where the id corresponds to playlist IDs in the subscription table.  
- Returns result with fetchAll the homepage videos-view.  

**uploadProgress.php**  
- Get users username $description  
- Get video info from Headers  
- Insert video in database  
- Returning data array with video information to the teachers manage video view.  

**uploadsubtitles.php**  
- Getting chosen video ID, name of file as a random string and filepath.  
- Moving chosen file to local storage.  
- Setting the subtitles column in the uploads table for the chosen video.  
- Returning data array with video information to the teachers manage video view.  

**uploadthumbnail.php**  
- Getting chosen video ID and converting chosen image to base64 string.  
- Giving file random name and creating correct filepath.  
- Create file variable and write picture to it.  
- Binding video id with thumbnail and uploading path to database.  
- Returning result to manage-videos-view.  

**videos.php**  
- Selecting all info about all videos currently uploaded to database.  
- Returning array of videos found with fetchAll to videos-view.  


### Dokumentasjon

#### Tre-struktur av applikasjonen

![Picture](images/clientside.png)

#### Wireframe skisser av ulike views

1. Hjemmesiden, hvor videoer vises for alle brukere.  
![Picture](images/videosview.jpg)

2. View av boksen som vises når en bruker trykker på person-ikonet øverst til høyre.  
![Picture](images/login.jpg)

3. Videoene som en enkelt lærer har lastet opp, med nyttig informasjon og muligheten til å redigere tittel og beskrivelse, og navigere til side for å endre thumbnail.  
![Picture](images/managevideosview.jpg)

4. Lærers side for å redigere en video sitt thumbnail eller laste opp nytt.  
![Picture](images/uploadthumbnail.jpg)

5. Siden med én enkelt video i utvidet format, med undertekster.  
![Picture](images/subtitlesview.jpg)

6. View for å registrere nye brukere. Statusmeldinger skal vises i popupboksen ved siden av boksene for valg av brukernavn og passord. Feilmeldingene inkluderer "Passord matcher ikke.", "Passord kan ikke vært blanke.", "Brukernavn kreves.", "Brukeren eksisterer allerede.".  
![Picture](images/register.jpg)

7. Denne siden er kun tilgjengelig for administratorer, og viser enten default person-ikon som bildet til brukeren eller egendefinerte avatarer. I tillegg står det brukernavn, bruker-ID og privilegietype (student, lærer eller admin).  
![Picture](images/admin.jpg)

8. Manage-user-view. Tilgjengelig fra admin-view for administrator ved valgt bruker, for å fjerne tilganger (gjøre til student), godkjenne bruker som lærer, heve privilegier til administratorstatus eller slette brukeren.  
![Picture](images/manageuserview.jpg)


### Mangler i prosjektet
- Undertekst som beveger seg ved siden av videoen i subtitles-view, og muligheten for å trykke på bestemte utsnitt for å hoppe dit i videoen.  
- Definisjon av egendefinerte tagger.
- Rekkefølgen på videoene som skal vises i spillelister kan ikke endres.


## Sjekkliste

☑ Felles pålogging for studenter, lærere og adminstratorer.  
☑ Ikke påloggede brukere skal kunne søke og se på videoer/spillelister på samme måte som påloggede brukere.  
☑ Lærere kan opprette og vedlikeholde spillelister (endre beskrivelse, legge til/fjerne videoer, endre rekkefølge på videoer.)  
☑ Lærere skal også kunne legge til videoer.  
☑ Legg også til mulighet til å velge en vilkårlig frame fra en video som thumbnail for videoen.  
☑ Det skal også være mulig å laste opp teksting av videoer.  
☑ Gi Lærere mulighet til å redigere video sin tittle og beskrivelse.
☑ Under redigering av video skal lærer kunne endre thumbnailbilde, endre tittel og beskrivelse og også laste opp ny fil som inneholder teksting av videoen.  
☐ Studenter skal kunne legge til favorittspillelister og få oversikt over nye videoer i disse spillelistene.  
☑ Administratorer kan endre status for brukere (dvs at når en bruker registrerer seg og ber om å bli lærer så skal administrator få en oversikt over dette og få mulighet til å gi disse lærertilgang.)  
☐ Ved visning av video skal tekstingen vises ved siden av videoen med aktuell tekst uthevet. Det skal være mulig å klikke på en del av teksten å få videoen til å hoppe direkte til den aktuelle delen av videoen.  
(Et godt eksempel på dette er bruken hos edX, anbefaler å melde seg på kurset «Introduksjon til JavaScript» (https://www.edx.org/course/javascript-introduction-w3cx-js-0x-0), kurset er gratis og viser både denne måten å bruke video på og lar dere laste ned videoer og tekstfiler med teksting (som enkelt kan gjøres om til VTT filer) som dere kan bruke som testfiler.)  
☑ Gi mulighet for å vise video i fullskjerm (sammen med tekstingen)  
☑ Video skal kunne vises i både høyere og lavere hastighet enn den vanlige (f.eks. dobbel hastighet og halv hastighet.)  

☑ Lage wireframe skisser av de ulike sidene i applikasjonen, dette gjøres enklest med penn og papir. (Bruk disse til å få en oversikt over funksjonaliteten til applikasjonen og hvordan denne kan implementeres.)  
☑ Ta bildet av skissene og legg dem inn i Wikien.  
☑ Alle klasser og funksjoner (både i PHP og JavaScript) skal selvfølgelig dokumenteres via kommentarer i koden. (I mange tilfeller hjelper det også på lesbarheten av koden om en kommenterer if/else if/else slik at det fremkommer tydelig hva hver enkelt grein faktisk gjør (hvorfor koden kjøres.) Noen ganger er dette åpenbart, andre ganger krever det mye finlesing av koden for å finne ut hva som egentlig skjer.)  
☑ Lage en oversikt i Wikien over alle endepunktene dere lager i PHP, dette blir dokumentasjonen av API’en til applikasjonen. For eksempel så vil dere ha en søkefunksjon, i fra nettsiden vil dere via AJAX kalle et PHP skript for å returnere videoer og spillelister som matcher et gitt søkekriterium. Dere må da beskrive hvilke parametere skriptet forventer og hva som returneres. Med en god dokumentasjon i PHP filene kan dette kopieres derfra. Skriv denne dokumentasjonen underveis i prosessen, dere vil trenge den selv under utviklingen av applikasjonen. Grupper de ulike PHP filene etter funksjon, dvs at det blir enkelt å finne de som har med brukerrelaterte operasjoner å gjøre (login, logut, registrere bruker, liste brukere, endre brukerstatus osv).  
☐ Lag en oversikt over alle egendefinerte tagger dere har laget, beskriv hvorfor de er laget, hvilke attributter som kan settes, hvilke funksjoner som en vil kalle og hvilke events som genereres fra disse.  
☑ Lag en tegning av trestrukturen som viser hvordan applikasjonen henger sammen på klientsiden. Dvs at dere tegner ut DOM strukturen for applikasjonen (finn et hensiktsmessig nivå å gjøre dette på, det blir meningsløst å ta med absolutt hele strukturen.) Ut ifra denne trestrukturen beskriver dere kort all funksjonalitet i applikasjonen.  
