SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(11) NOT NULL,
  `owner` text NOT NULL,
  `fileName` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `filePath` text NOT NULL,
  `thumbnail` text DEFAULT NULL,
  `noOfVotes` int(11) NOT NULL,
  `avgRating` float NOT NULL,
  `totalScore` int(11) NOT NULL,
  `playlistParent` int(11) DEFAULT NULL,
  `subtitles` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `owner`, `fileName`, `title`, `description`, `filePath`, `thumbnail`, `noOfVotes`, `avgRating`, `totalScore`, `playlistParent`, `subtitles`) VALUES
(69, 'teacher1@a.no', 'ciEGZn.mp4', 'Interaktiv Bash Scripting', 'Vi driver med bash scripting i fellesskap', '../uploads/ciEGZn.mp4', NULL, 1, 4, 4, 35, '../subs/yas.vtt'),
(70, 'teacher1@a.no', 'cXDTXL.mp4', 'Intro Bash', 'Lett introduksjon til scripting i bash', '../uploads/cXDTXL.mp4', NULL, 1, 2, 2, 35, NULL),
(71, 'teacher1@a.no', 'XsmFHa.mp4', 'Prosesser og trÃ¥der', 'C-programmering med trÃ¥der', '../uploads/XsmFHa.mp4', NULL, 0, 0, 0, NULL, NULL),
(72, 'teacher1@a.no', 'bN29cr.mp4', 'Flexbox', 'Vi gjÃ¸r en gjennomgang av hvordan bruke flexbox', '../uploads/bN29cr.mp4', NULL, 3, 4, 12, 34, NULL),
(73, 'teacher1@a.no', '2P9s6E.mp4', 'CSS Grid', 'Eksempel pÃ¥ hvordan vi kan bruke Grid for Ã¥ dele opp en nettside. ', '../uploads/2P9s6E.mp4', NULL, 3, 3.66667, 11, 34, NULL),
(74, 'teacher1@a.no', 'ZVcPIc.mp4', 'Concurrency', 'Java and Android concurrency', '../uploads/ZVcPIc.mp4', NULL, 1, 4, 4, 36, NULL),
(75, 'teacher1@a.no', 'YW8NHE.mp4', 'Android', 'Persistence, file system, internal storage, SQLite', '../uploads/YW8NHE.mp4', NULL, 1, 4, 4, 36, NULL),
(76, 'teacher1@a.no', 'oLiEOK.mp4', 'Twig', 'Gjennomgang av hvordan vi setter opp twig miljÃ¸ i php', '../uploads/oLiEOK.mp4', NULL, 1, 5, 5, 37, NULL),
(77, 'teacher1@a.no', 'tg8cCP.mp4', 'Php syntax', 'Gjennomgang av litt enkel php', '../uploads/tg8cCP.mp4', NULL, 1, 4, 4, 37, NULL),
(78, 'teacher1@a.no', 'yRE6sM.mp4', 'CSS Tutorial', 'Steg for steg beskrivelse.', '../uploads/yRE6sM.mp4', '../thumbnails/2xjuIS.jpg', 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE `playlists` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `owner` text NOT NULL,
  `subject` text NOT NULL,
  `theme` text NOT NULL,
  `thumbnail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playlists`
--

INSERT INTO `playlists` (`id`, `title`, `description`, `owner`, `subject`, `theme`, `thumbnail`) VALUES
(34, 'WWW-teknologi Uke 2', 'Forelesninger som inneholder gjennomgang av eksempler fra boka. ', 'teacher1@a.no', 'IMT2291 WWW-teknologi', 'HTML og CSS', '../thumbnails/rNJPJZ.jpg'),
(35, 'Operativsystemer Uke 4', 'Diverse gjennomgang av bash scripting gjort i forelesningene', 'teacher1@a.no', 'IMT2282 Operativsystemer', 'Bash scripting', '../thumbnails/1dMCXY.jpg'),
(36, 'Mobile and Wearable Week 5', 'We work more in android studio', 'teacher1@a.no', 'IMT3673 Mobile and Wearable Programming', 'Android Studio', '../thumbnails/rNETGX.jpg'),
(37, 'PHP gjenoppfriskning', 'Se gjennom for Ã¥ friske opp PHP kunnskapen', 'teacher1@a.no', 'IMT2291 WWW-teknologi', 'PHP, Twig', '../thumbnails/C17Q6Y.jpg');

-- -------------------

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uname` varchar(64) NOT NULL,
  `type` ENUM('student','teacher','admin') NOT NULL DEFAULT 'student',
  `pwd` varchar(130) NOT NULL,
  `avatar` blob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `uname`, `type`, `pwd`, `avatar`) VALUES (1, 'student', 'student', MD5('student'), '../avatars/a.jpg');
INSERT INTO `user` (`id`, `uname`, `type`, `pwd`, `avatar`) VALUES (2, 'laerer', 'teacher', MD5('laerer'), NULL);
INSERT INTO `user` (`id`, `uname`, `type`, `pwd`, `avatar`) VALUES (3, 'admin', 'admin', MD5('admin'), '../avatars/c.png');
INSERT INTO `user` (`id`, `uname`, `type`, `pwd`, `avatar`) VALUES (4, 'teacher1@a.no', 'teacher', MD5('laerer'), '../avatars/b.jpg');


ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
