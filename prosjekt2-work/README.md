### Usage

Make sure you do not have any conflicting volumes, run : docker volume prune

Download dependencies with npm, go into the www folder (on the host machine) and run: npm install

First time usage, rebuild everything : docker-compose up -d --build

After that, start with : docker-compose up -p

Go to : http://localhost:8080 to view the page

Run test : docker-compose exec test bash
then : codecept run

NOTE: comment out the correct line 76/77 depending on if you want to run tests
or view the page on localhost:8080


Hvis du har en database som ikke er oppdatert bruker du kommandoene:

  - docker-compose down
  - docker volume rm prosjekt2-work_persistent

Etter det kan du bygge prosjektet igjen ved bruk av docker-compose up -d --build
