<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Selecting all info about all videos currently uploaded to database.
$stmt = $db->prepare('SELECT * FROM uploads');

$stmt->execute(array());

// Returning array of videos found with fetchAll to videos-view.
echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
