<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

$videoid = $_POST['videoid'];
$playlistid = $_POST['playlistid'];

$stmt = $db->prepare("UPDATE uploads SET playlistParent = :playlistid WHERE id = :videoid");
$stmt->bindParam(":playlistid", $playlistid);
$stmt->bindParam(":videoid", $videoid);
$stmt->execute();

$data['status'] = "success";

echo json_encode($data);
