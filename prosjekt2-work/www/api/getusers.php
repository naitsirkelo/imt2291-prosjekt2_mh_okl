<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Getting user id, username, type and avatar (default = null) of all users.
$stmt = $db->prepare("SELECT id, uname, type FROM user");

// Executing statement to array.
$stmt->execute(array());

// Returning as fetchAll to show all users in admin-view.
echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
