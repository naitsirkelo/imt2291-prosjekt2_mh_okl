<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Getting info about all playlists from database.
$stmt = $db->prepare('SELECT * FROM playlists');

$stmt->execute(array());

// Returning array with fetchAll to playlists-view.
echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
