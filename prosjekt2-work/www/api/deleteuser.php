<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get ID and avatar path of chosen user.
$userid = $_POST['userid'];

// Try to delete user from database.
$query = $db->prepare("DELETE FROM user WHERE id = :id");
$query->bindParam(":id", $userid);
$query->execute();

$data['stats'] = 'User deleted.';
// Return custom data array with row count.
echo json_encode($data);
