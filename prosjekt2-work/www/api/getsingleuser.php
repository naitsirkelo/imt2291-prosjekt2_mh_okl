<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get chosen user ID from admin-view.
$id = $_POST['id'];

$stmt = $db->prepare("SELECT id, uname, type FROM user WHERE id=:id");
$stmt->bindParam(':id', $id);
$stmt->execute();

// Returns user id, username, type and avatar in manage-user-view.
echo json_encode($stmt->fetch(PDO::FETCH_ASSOC));
