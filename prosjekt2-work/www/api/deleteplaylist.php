<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get chosen playlist ID and thumbnail, from manage-playlists-view.
$playlistid = $_POST['playlistid'];
$thumbnail = $_POST['thumbnail'];

$stmt = $db->prepare("DELETE FROM playlists WHERE id = :id");
$stmt->bindParam(":id", $playlistid);
$stmt->execute();

// Store result as a row count in a custom data array.
$data['deleterows'] = $stmt->rowCount();

$query = $db->prepare("UPDATE uploads SET playlistParent = null WHERE playlistParent = :id");
$query->bindParam(":id", $playlistid);
$query->execute();

$data['updatevideorows'] = $query->rowCount();

// Deleting local storage at thumbnail path.
unlink($thumbnail);

// Returns the updated row count to manage view.
echo json_encode($data);
