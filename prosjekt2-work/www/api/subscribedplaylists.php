<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Gets current student user ID.
$uid = $_POST['uid'];

// Gets playlists where the id corresponds to playlist IDs in the subscription table.
$stmt = $db->prepare("SELECT * FROM playlists WHERE id IN (SELECT playlistid FROM subscriptions WHERE studentid=:uid)");
$stmt->bindParam(":uid", $uid);
$stmt->execute();

// Returns result with fetchAll the homepage videos-view.
echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
