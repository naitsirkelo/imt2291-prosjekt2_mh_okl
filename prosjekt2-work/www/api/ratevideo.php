<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

$videoid = $_POST['videoid'];  //Get the videoid and rating given
$rating = $_POST['rating'];

// Finds current users username.
$stmt = $db->prepare("SELECT uname FROM user WHERE id=:id");
$stmt->bindParam(":id", $_SESSION['uid']);
$stmt->execute();
$res = $stmt->fetch(PDO::FETCH_ASSOC);
$uname = $res['uname'];

//Insert rating into rating table
$query = $db->prepare("INSERT INTO ratings (username, videoID, rating) VALUES (?, ?, ?)");
$query->execute(array($uname, $videoid, $rating));

//Get the totalScore and noOfVotes from db for video with id
$query = $db->prepare("SELECT noOfVotes, totalScore FROM uploads WHERE id=:id");
$query->bindParam(":id", $videoid);
$query->execute();
$votes = $query->fetchAll();

$noOfVotes = $votes[0]['noOfVotes'];
$totalScore = $votes[0]['totalScore'];
$noOfVotes += 1;    //Add 1 to noOfVotes
$totalScore += $rating; //Add rating to total score
$avgRating = $totalScore / $noOfVotes; //Calculate avg

$query = $db->prepare("UPDATE uploads SET noOfVotes = :noOfVotes, avgRating = :avgRating, totalScore = :totalScore WHERE id = :id");
$query->bindParam(":id", $videoid);
$query->bindParam(":noOfVotes", $noOfVotes);
$query->bindParam(":avgRating", $avgRating);
$query->bindParam(":totalScore", $totalScore);
$query->execute();

$data['status'] = "success";

echo json_encode($data);
