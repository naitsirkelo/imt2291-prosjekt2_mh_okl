<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get chosen video ID and path, and path to thumbnail if set.
$videoid = $_POST['videoid'];
$videothumbnail = $_POST['thumbnail'];
$videopath = $_POST['filePath'];
$subtitles = $_POST['subtitles'];

$query = $db->prepare("DELETE FROM uploads WHERE id = :id");
$query->bindParam(":id", $videoid);
$query->execute();

$data['rows'] = $query->rowCount();

// Delete locally stored video and, if it exists, thumbnail.
unlink($videopath);
if(file_exists($videothumbnail)) {
  unlink($videothumbnail);
}

if(file_exists($subtitles)) {
  unlink($subtitles);
}

// Return custom data array with row count.
echo json_encode($data);
