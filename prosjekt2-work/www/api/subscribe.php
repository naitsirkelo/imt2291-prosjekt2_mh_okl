<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get chosen user and playlist ID.
$uid = $_POST['uid'];
$pid = $_POST['playlistid'];

// Try to get the already stored subscription from database. If it exists, return error message.
try {
  $stmt = $db->prepare('SELECT * FROM subscriptions WHERE studentid=:uid AND playlistid=:pid');
  $stmt->bindParam(':uid', $uid);
  $stmt->bindParam(':pid', $pid);
  $stmt->execute();
  $res = $stmt->fetch(PDO::FETCH_ASSOC);
}
catch(PDOException $e) {
  $result['status'] = "Error fetching data.";
}

if (!$res) {
  // Insert new subscription, if not already existing.
  $stmt = $db->prepare("INSERT INTO subscriptions (studentid, playlistid) VALUES (?, ?)");
  $stmt->execute(array($uid, $pid));

  // Returning result status to playlists-view.
  $result['status'] = 'Subscription added.';
} else {
  $result['status'] = 'You are already subscribed.';
}
echo json_encode($result);
