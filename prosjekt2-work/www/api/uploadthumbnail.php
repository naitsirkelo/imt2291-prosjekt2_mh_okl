<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Getting chosen video ID and converting chosen image to base64 string.
$videoid = $_POST['videoid'];
$img = $_POST['img'];
$img = str_replace(' ','+',$img);
$img = substr($img, strpos($img,",")+1);
$img = base64_decode($img);

// Giving file random name and creating correct filepath.
$fileName = randomString();
$filepath = "../thumbnails/" . $fileName;

$file = fopen($filepath, 'w');  // Create file and write pic to it
fwrite($file, $img);
fclose($file);

// Binding video id with thumbnail and uploading path to database.
$query = $db->prepare("UPDATE uploads SET thumbnail = :thumbnail WHERE id= :id");
$query->bindParam(":id", $videoid);
$query->bindParam(":thumbnail", $filepath);
$query->execute();

$data['status'] = "OK";

// Returning result to manage-videos-view.
echo json_encode($data);


function randomString() {  //Creates random file name
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < 6; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString . '.jpeg';
  }
