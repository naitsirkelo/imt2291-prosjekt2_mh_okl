<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get chosen inputs from fields.
$username = $_POST['uname'];
$password = $_POST['pwd'];
$password2 = $_POST['pwd2'];

if ($password == "" || $password2 == "") {
  $res['msg'] = "Passwords can't be blank.";
} else {
  if ($password == $password2) {
    if ($username != "") {
      $stmt = $db->prepare('SELECT id FROM user WHERE uname=:uname');
      $stmt->bindParam(':uname', $username);
      $stmt->execute();
      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      if (!$result) { // If no user found, username is available.
        $stmt = $db->prepare('INSERT INTO user (uname, type, pwd, avatar) VALUES (?, ?, ?, ?)');
        $stmt->execute(array($username, 'student', password_hash($password, PASSWORD_DEFAULT), NULL));  // Convert md5 password to password_hash password
        $res['msg'] = "User registered successfully.";
      } else {
        $res['msg'] = "User already exists.";
      }
    } else {
      $res['msg'] = "Please enter a username.";
    }
  } else {
  $res['msg'] = "Passwords doesn't match.";
  }
}

echo json_encode($res);
