<?php
session_start();

require_once 'classes/DB.php';
$db = DB::getDBConnection();
$uid = $_SESSION['uid'];


$fileName = randomString();
$filePath = '../uploads/' . $fileName;


$handle = fopen('php://input', 'r');                // Read the file from stdin
$output = fopen($filePath, 'w');
$contents = '';

while (!feof($handle)) {                            // Read in blocks of 8 KB (no file size limit)
    $contents = fread($handle, 8192);
    fwrite($output, $contents);
}
fclose($handle);
fclose($output);
$data['size'] = filesize($filePath);
//

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin, x-title, x-description, content-type");
header("Access-Control-Allow-Credentials: true");
header("Content-type: application/json");           // Send back json data

//Get users username $description
$stmt = $db->prepare("SELECT uname FROM user WHERE id=$uid");
$stmt->execute();
$res = $stmt->fetch(PDO::FETCH_ASSOC);
$uname = $res['uname'];

//Get video info from Headers
$title = $_SERVER['HTTP_X_TITLE'];
$description = $_SERVER['HTTP_X_DESCRIPTION'];

//Insert video in database
$stmt =$db->prepare("INSERT INTO uploads (owner, fileName, description, filePath, title, totalScore, noOfVotes, avgRating) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
$stmt->execute(array($uname, $fileName, $description, $filePath, $title, 0, 0, 0));

$data['userName'] = $uname;
$data['fileName'] = $fileName;
$data['title'] = $title;
$data['description'] = $description;

// Returning data array with video information to the teachers manage video view.
echo json_encode($data);



function randomString() {  //Creates random file name
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < 6; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString . '.mp4';
  }
