<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get chosen video ID in videos-view.
$id = $_POST['id'];

$stmt = $db->prepare("SELECT * FROM uploads WHERE id=:id");
$stmt->bindParam(':id', $id);
$stmt->execute();

// Returning all info about video to either subtitles-view or upload-thumbnail-view.
echo json_encode($stmt->fetch(PDO::FETCH_ASSOC));
