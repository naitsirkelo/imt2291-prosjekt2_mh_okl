<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");

error_reporting( E_ALL );
ini_set('display_errors', 1);

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Get chosen user ID.
$id = $_POST['id'];

// Update selected ID to give the user only student privileges.
$stmt = $db->prepare("UPDATE user SET type=? WHERE id=?");
$stmt->execute(array('student', $id));

// Returning result status to manage-user-view.
$result['status'] = 'Privileges removed.';
echo json_encode($result);
