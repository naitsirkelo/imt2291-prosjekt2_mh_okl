<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");


require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Gets current user ID (- only available for teachers).
$uid = $_SESSION['uid'];

// Finds current users username.
$stmt = $db->prepare("SELECT uname FROM user WHERE id=$uid");
$stmt->execute();
$res = $stmt->fetch(PDO::FETCH_ASSOC);
$uname = $res['uname'];

// Gets videos with the user set as owner.
$stmt = $db->prepare("SELECT * FROM uploads WHERE owner=:username");
$stmt->bindParam(":username", $uname);
$stmt->execute();

// Returns result with fetchAll to manage-videos-view.
echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
