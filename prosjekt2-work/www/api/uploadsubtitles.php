<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Getting chosen video ID, name of file as a random string and filepath.
$data['videoid'] = $_POST['videoid'];
$data['fileName'] = randomString();
$data['filePath'] = "../subs/" . $data['fileName'];

// Moving chosen file to local storage.
move_uploaded_file($_FILES['file']['tmp_name'], $data['filePath']);

// Setting the subtitles column in the uploads table for the chosen video.
$query = $db->prepare("UPDATE uploads SET subtitles=:subtitles WHERE id=:id");
$query->bindParam(":subtitles", $data['filePath']);
$query->bindParam(":id", $data['videoid']);
$query->execute();

// Returning data array with video information to the teachers manage video view.
echo json_encode($data);


function randomString() {  //Creates random file name
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < 6; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString . '.vtt';
  }
