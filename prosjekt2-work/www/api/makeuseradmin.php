<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Credentials: true");

error_reporting( E_ALL );
ini_set('display_errors', 1);

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Gets ID from manage-user-view.
$id = $_POST['id'];

// Update selected ID to give chosen user admin privileges.
$stmt = $db->prepare("UPDATE user SET type=? WHERE id=?");
$stmt->execute(array('admin', $id));

// Returning result status.
$result['status'] = 'Admin approved.';
echo json_encode($result);
