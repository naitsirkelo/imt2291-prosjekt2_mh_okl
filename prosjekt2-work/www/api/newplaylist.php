<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Gets current user ID (- only available for teachers).
$uid = $_SESSION['uid'];

// Finds current users username.
$stmt = $db->prepare("SELECT uname FROM user WHERE id=$uid");
$stmt->execute();
$res = $stmt->fetch(PDO::FETCH_ASSOC);
$uname = $res['uname'];

// Getting chosen variables from manage-playlists-view.
$title = $_POST['title'];
$description = $_POST['description'];
$subject = $_POST['subject'];
$theme = $_POST['theme'];
$thumbnailname = substr($_FILES["file"]["tmp_name"], -6) . '.jpg';
$thumbnailpath = "../thumbnails/" . $thumbnailname;

// Storing chosen file in local thumbnail directory.
move_uploaded_file($_FILES["file"]["tmp_name"], $thumbnailpath);

$data['thumb'] = $thumbnailpath;

// Insert new row with playlist into database.
$stmt = $db->prepare("INSERT INTO playlists (owner, thumbnail, description, subject, theme, title) VALUES (?, ?, ?, ?, ?, ?)");
$stmt->execute(array($uname, $thumbnailpath, $description, $subject, $theme, $title));

$data['status'] = "SUCCESS";
// Returning data array with thumbnail path and status to manage-playlists-view.
echo json_encode($data);
