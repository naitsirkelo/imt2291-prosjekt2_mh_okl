<?php
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://www" || $http_origin == "http://localhost:8080") {
    header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

require_once 'classes/DB.php';
$db = DB::getDBConnection();

// Store criteria variable from the search input field on the homepage videos-view.
$criteria = $_POST['criteria'];

// Get video item from database with any column matching criteria of search.
$stmt = $db->prepare("SELECT * FROM uploads WHERE title LIKE '%$criteria%'
                                        OR description LIKE '%$criteria%'
                                        OR owner LIKE '%$criteria%'
                                        OR fileName LIKE '%$criteria%'");

$stmt->execute();

// Returning data array with found videos to an updated videos-view.
echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
