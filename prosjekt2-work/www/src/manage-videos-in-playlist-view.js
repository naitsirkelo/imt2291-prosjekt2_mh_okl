import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';
import '@vaadin/vaadin-button/vaadin-button.js'


/*
Accessable only by teachers, from the homepage, videos-view.
Lets the teacher edit the playlist and videos in it.
*/
class ManageVideosInPlaylistView extends LitElement {
  static get properties () {
    return {
      videosinplaylist: {
        type: Array
      },
      videosnotinplaylist: {
        type: Array
      },
      user: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      playlistid: {
        type: String
      }
    }
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }


    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 320px 320px 320px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 320px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }


    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }


    `;
  }

  constructor () {
    super();
    this.videosinplaylist = [];
    this.videosnotinplaylist = [];

    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
      console.log(this.user);
    });
  }

  render() {
    return html`
    <style include="shared-styles">
      :host {
        display: block;

        padding: 10px;
      }
    </style>
    <a href="${MyAppGlobals.rootPath}manageplaylists" @click="${this.minimize}">Tilbake</a>
    <vaadin-button @click="${this.getVideosInPlaylist}">Videoer i spillelisten</vaadin-button>
    <vaadin-button @click="${this.getVideosNotInPlaylist}">Videoer ikke i spillelisten</vaadin-button>
    <div class="grid-outer">
      <div id="inplaylist" style="height: 0px; visibility: hidden;">
        <div class="grid-inner">
        ${this.videosinplaylist.map(video => {
          return html`
          <div class="video">
          ${video.thumbnail?
            html`
            <video controls width="320" height="240" poster="${video.thumbnail}">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `:
            html`
            <video controls width="320" height="240">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `}
          <label for="title"><br>Tittel: ${video.title} <br> </label>
          <label for="description">Beskrivelse: ${video.description} <br> </label>
          <form onsubmit="javascript: return false;">
            <input type="hidden" name="videoid" value="${video.id}" />
            <button @click="${this.removeVideo}">Fjern fra spilleliste</button>
          </form>
          </div>
        `;
        })}
        </div>
      </div>
      <div id="notinplaylist" style="height: 0px; visibility: hidden;">
        <div class="grid-inner">
        ${this.videosnotinplaylist.map(video => {
          return html`
          <div class="video">
          ${video.thumbnail?
            html`
            <video controls width="320" height="240" poster="${video.thumbnail}">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `:
            html`
            <video controls width="320" height="240">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `}
          <label for="title"><br>Tittel: ${video.title} <br> </label>
          <label for="description">Beskrivelse: ${video.description} <br> </label>
          <form onsubmit="javascript: return false;">
            <input type="hidden" name="videoid" value="${video.id}" />
            <button @click="${this.addVideo}">Legg til i spilleliste</button>
          </form>
          </div>
        `;
        })}
        </div>
    </div>

    `;
  }

  getVideosInPlaylist() {
    var url_string = document.URL;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    this.playlistid = id;
    var target = this.shadowRoot.getElementById('inplaylist');
    var other = this.shadowRoot.getElementById('notinplaylist');
    if(target.style.visibility == "hidden") {
      target.style.visibility="visible";
      target.style.height="auto";
      other.style.height="0px";   //Hide the other form
      other.style.visibility="hidden";
    } else {  //If visible, set to hidden
      target.style.visibility="hidden";
      target.style.height="0px";
    }
    const data = new FormData();
    data.append("playlistid", id);
    fetch (`${window.MyAppGlobals.serverURL}api/getvideosinplaylist.php`, {
      method: 'POST',
      credentials: 'include',
      body: data
    }
    ).then(res=>res.json())
    .then(data=>{
      this.videosinplaylist = data;
    });
  }

  addVideo(e) {
    const data = new FormData(e.target.form);
    data.append("playlistid", this.playlistid);
    fetch (`${window.MyAppGlobals.serverURL}api/addvideo.php`, {
      method: 'POST',
      credentials: 'include',
      body: data
    }
    ).then(res=>res.json())
    .then(data=>{
      console.log(data);
      const data1 = new FormData();
      data1.append("playlistid", this.playlistid);
      fetch (`${window.MyAppGlobals.serverURL}api/getvideosnotinplaylist.php`, {
        method: 'POST',
        credentials: 'include',
        body: data1
      }
      ).then(res=>res.json())
      .then(data=>{
        this.videosnotinplaylist = data;
      });
    });
  }

  removeVideo(e) {
    const data = new FormData(e.target.form);
    fetch (`${window.MyAppGlobals.serverURL}api/removevideo.php`, {
      method: 'POST',
      credentials: 'include',
      body: data
    }
    ).then(res=>res.json())
    .then(data=>{
      console.log(data);
      const data1 = new FormData();
      data1.append("playlistid", this.playlistid);
      fetch (`${window.MyAppGlobals.serverURL}api/getvideosinplaylist.php`, {
        method: 'POST',
        credentials: 'include',
        body: data1
      }
      ).then(res=>res.json())
      .then(data=>{
        this.videosinplaylist = data;
      });
    });
  }

  getVideosNotInPlaylist() {
    var url_string = document.URL;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    this.playlistid = id;
    var target = this.shadowRoot.getElementById('notinplaylist');
    var other = this.shadowRoot.getElementById('inplaylist');
    if(target.style.visibility == "hidden") {
      target.style.visibility="visible";
      target.style.height="auto";
      other.style.height="0px";   //Hide the other form
      other.style.visibility="hidden";
    } else {  //If visible, set to hidden
      target.style.visibility="hidden";
      target.style.height="0px";
    }
    const data = new FormData();
    data.append("playlistid", id);
    fetch (`${window.MyAppGlobals.serverURL}api/getvideosnotinplaylist.php`, {
      method: 'POST',
      credentials: 'include',
      body: data
    }
    ).then(res=>res.json())
    .then(data=>{
      this.videosnotinplaylist = data;
    });
  }

  minimize() {
    var target = this.shadowRoot.getElementById('inplaylist');
    var other = this.shadowRoot.getElementById('notinplaylist');
    target.style.visibility="hidden";
    other.style.visibility="hidden";
  }


}

customElements.define('manage-videos-in-playlist-view', ManageVideosInPlaylistView);
