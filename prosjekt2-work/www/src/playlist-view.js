import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';

/*
  Accessable by all users, logged in or not, from the homepage.
  Lets user watch the chosen video with subtitles and a playback speed option.
*/
class PlaylistView extends LitElement {
  static get properties () {
    return {
      user: { type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      videos: { type: Array },
      playlistid: { type: String },
      playlist: { type: Object }
    };
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }

    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 320px 320px 320px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 320px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }

    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    `;
  }

  /*
   Declaring empty playlist object and videos array, as well as getting the current playlist ID.
   */
  constructor () {
    super();
    this.playlist = {};
    this.videos = [];
    var url_string = document.URL;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    this.playlistid = id;
    this.getplaylist();
    this.getvideos();

    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
    });
  }

  render() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
      </style>
    <a href="${MyAppGlobals.rootPath}videosview">Tilbake</a>
      <div class="playlist">
        <h2>${this.playlist.title}</h2>
        <label for="owner">Lærer: ${this.playlist.owner}<br></label>
        <label for="description">${this.playlist.description}<br></label>
      </div>
      <div class="grid-outer">
        <div class="grid-inner">
        <!-- Showing each video in the map with their own thumbnail, controls, etc. -->
        ${this.videos.map(video => {
          return html`
          <div class="video">
            ${video.thumbnail?
              html`
              <video controls width="320" height="240" poster="${video.thumbnail}">
                <source src="${video.filePath}" type="video/mp4">
              </video>
              `:
              html`
              <video controls width="320" height="240">
                <source src="${video.filePath}" type="video/mp4">
              </video>
              `}
            <!-- Link to open view to watch chosen video with subtitles. -->
            <a href="${MyAppGlobals.rootPath}subtitlesview?id=${video.id}">Se med undertekster</a>
            <label for="title"><br>Tittel: ${video.title} <br> </label>
            <label for="description">Beskrivelse: ${video.description} <br> </label>
            <label for="fileName">Filnavn: ${video.fileName} <br> </label>
            <label for="score">Rating: ${video.avgRating} <br> </label>
            <label for="owner">Lastet opp av: ${video.owner} <br> </label>
          </div>
          `;
        })}
        </div>
      </div>
    `;
  }

  /*
   * Called from constructor, to load the currently chosen playlist information.
   */
  getplaylist() {
    const data1 = new FormData();
    data1.append('id', this.playlistid);
    fetch (`${window.MyAppGlobals.serverURL}api/getsingleplaylist.php`, {
      method: 'POST',
      credentials: 'include',
      body: data1
    }).then(res=>res.json())
    .then(data=>{
      this.playlist = data;
    });
  }

  /*
   * Called from constructor, to load videos under the playlist chosen.
   */
  getvideos() {
    const data1 = new FormData();
    data1.append('id', this.playlistid);
    fetch (`${window.MyAppGlobals.serverURL}api/getplaylistvideos.php`, {
      method: 'POST',
      credentials: 'include',
      body: data1,
      mode: 'cors'
    }).then(res=>res.json())
    .then(data=>{
      this.videos = data;
    });
  }
}

customElements.define('playlist-view', PlaylistView);
