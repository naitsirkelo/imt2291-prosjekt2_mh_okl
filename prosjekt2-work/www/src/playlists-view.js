import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';

/*
  Accessable by all users, logged in or not, from the homepage.
  Lets users watch navigate to videos within existing playlists.
*/
class PlaylistsView extends LitElement {
  static get properties () {
    return {
      user: { type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      playlists: { type: Array },
      searchbool: { type: Boolean },
      msg: { type: String }
    }
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      float: left;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }

    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 320px 320px 320px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 320px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }

    `;
  }

  constructor () {
    super();
    this.playlists = [];
    this.msg = '';
    fetch (`${window.MyAppGlobals.serverURL}api/playlists.php`)
    .then(res=>res.json())
    .then(data=>{
      this.playlists = data;
    });

    const data1 = store.getState();
    this.user = data1.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
    });
  }

  render() {
    return html`
    <style include="shared-styles">
      :host {
        display: block;

        padding: 10px;
      }
    </style>

    <div class="card">
      <h3>Søk etter spilleliste</h3>
      <form onsubmit="javascript: return false;">
        <label>
          <input class="inputfield" type="text" name="criteria" placeholder="Søk etter emne, tema, tittel etc...">
          <button class="button" @click="${this.search}">Søk</button>
        </label>
      </form>
    </div>
    <h1>Spillelister</h1><label for="msg">${this.msg}</label>
    <div class="grid-outer">
      <div class="grid-inner">
      ${this.playlists.map(playlist => {
        return html`
        <div class="playlist">
          <img src="${playlist.thumbnail}" height="240" width="320">
          <label for="title"><br>Tittel: ${playlist.title} <br> </label>
          <label for="description">Beskrivelse: ${playlist.description} <br> </label>
          <label for="subject">Emne: ${playlist.subject} <br> </label>
          <label for="theme">Tema: ${playlist.theme} <br> </label>
          <label for="owner">Lastet opp av: ${playlist.owner} <br> </label>
          <!-- Link to open view to watch chosen video with subtitles. -->
          <a href="${MyAppGlobals.rootPath}playlistview?id=${playlist.id}">Se videoer</a>
          ${this.user.isStudent?    //Hvis brukeren er student
            html`
            <form method="post">
              <input type="hidden" name="playlistid" value="${playlist.id}" />
              <button class="button" style="float: right;" @click="${this.subscribe}">Abonner</button>
            </form>
            `:
            html``
          }
        </div>
        `;
      })}
      </div>
    </div>
    `;
  }

  searchbuttonclick(e) {
    this.searchbool = !this.searchbool;
  }

  /**
   * Called when the user clicks the search button after they enter a search criteria.
   *
   * @param  {Object} e event object that contains the search criteria.
   */
  search(e) {
    const data = new FormData(e.target.form);
    fetch(`${window.MyAppGlobals.serverURL}api/searchplaylist.php`, {
        method: 'POST',
        credentials: 'include',
        body: data
    }).then(res=>res.json())
    .then(data=>{
    this.playlists = data;
    });
  }

  /**
   * Called when the student clicks the 'Abonner' button below the playlist thumbnail.
   *
   * @param  {Object} e event object that contain playlist info.
   */
  subscribe(e) {
    e.preventDefault();
    const data = new FormData();
    var pid = e.target.form.playlistid.value;

    data.append('uid', this.user.uid);
    data.append('playlistid', pid);
    fetch(`${window.MyAppGlobals.serverURL}api/subscribe.php`, {
        method: 'POST',
        credentials: 'include',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      this.msg = data['status'];
    });
  }
}

customElements.define('playlists-view', PlaylistsView);
