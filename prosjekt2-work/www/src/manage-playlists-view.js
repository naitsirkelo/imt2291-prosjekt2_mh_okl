import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';
import '@vaadin/vaadin-button/vaadin-button.js'


/*
  Accessable only by teachers, from the homepage, videos-view.
  Lets the teacher edit the playlist and videos in it.
*/
class ManagePlaylistsView extends LitElement {
  static get properties () {
    return {
      playlists: {
        type: Array
      },
      user: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      }
    }
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }


    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 320px 320px 320px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 320px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }


    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }


    `;
  }

  constructor () {
    super();
    this.playlists = [];
    fetch (`${window.MyAppGlobals.serverURL}api/myplaylists.php`, {
      credentials: 'include'
    }
    ).then(res=>res.json())
    .then(data=>{
      this.playlists = data;
    });
    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
      console.log(this.user);
    });
  }

  render() {
    return html`
    <h1>Administrer spillelister</h1>
    <div class="grid-outer">
    <vaadin-button @click="${this.showUpload}">Lag ny spilleliste</vaadin-button>
    <div id="playlistform"  style="visibility: hidden; height: 0px">
    <form id="newplaylist" method="post" enctype="multipart/form-data">
      <div>
        <label for="title">Tittel</label>
        <input type="text" name="title" required />
      </div>
      <div>
        <label for="description">Beskrivelse</label>
        <input type="text" name="description" required />
      </div>
      <div>
        <label for="subject">Emne</label>
        <input type="text" name="subject" required />
      </div>
      <div>
        <label for="theme">Tema</label>
        <input type="text" name="theme" required />
      </div>
      <div>
        <label for="file">Velg thumbnail</label>
        <input type="file" name="file" accept="image/*" required />
      </div>
      <button @click="${this.createPlaylist}">Lagre spilleliste</button>
    </form>
    </div>
      <div class="grid-inner">
      ${this.playlists.map(playlist => {
        return html`
        <div class="playlist">
        <form onsubmit="javascript: return false;">
          <input type="hidden" name="playlistid" value="${playlist.id}" />
          <input type="hidden" name="thumbnail" value="${playlist.thumbnail}" />
          <button @click="${this.deleteplaylist}" style="float: right">X</button>
        </form>
          <img src="${playlist.thumbnail}" width="320" height="240">
          <label for="title"><br>Tittel: ${playlist.title} <br> </label>
          <label for="description">Beskrivelse: ${playlist.description} <br> </label>
          <label for="fileName">Emne: ${playlist.subject} <br> </label>
          <label for="score">Tema: ${playlist.theme} <br> </label>
          <label for="owner">Lastet opp av: ${playlist.owner} <br> </label>
          <a href="${MyAppGlobals.rootPath}managevideosinplaylistview?id=${playlist.id}">Legg til/fjern videoer</a>
        </div>
        `;
      })}
      </div>
    </div>
    `;
  }

  deleteplaylist(e) {
    const data = new FormData(e.target.form);
    fetch(`${window.MyAppGlobals.serverURL}api/deleteplaylist.php`, {
        method: 'POST',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      fetch (`${window.MyAppGlobals.serverURL}api/myplaylists.php`, {
        credentials: 'include'
      }
      ).then(res=>res.json())
      .then(data=>{
        this.playlists = data;
      });
    });
  }

  showUpload(e) { //toggle or untoggle form for playlist upload
    var form = this.shadowRoot.getElementById('playlistform'); //Get the form element
    if(form.style.visibility == "hidden") { //If hidden, set to visible
      form.style.visibility="visible";
      form.style.height="auto";
    } else {  //If visible, set to hidden
      form.style.visibility="hidden";
      form.style.height="0px";
    }
  }

  createPlaylist(e) {
    e.preventDefault();
    const data = new FormData(e.target.form);
    fetch(`${window.MyAppGlobals.serverURL}api/newplaylist.php`, {
        method: 'POST',
        credentials: 'include',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      fetch (`${window.MyAppGlobals.serverURL}api/myplaylists.php`, {
        credentials: 'include'
      }).then(res=>res.json())
      .then(data=>{
        this.playlists = data;
      });
    });
  }
}

customElements.define('manage-playlists-view', ManagePlaylistsView);
