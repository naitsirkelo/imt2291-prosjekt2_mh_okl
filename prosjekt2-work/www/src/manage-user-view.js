import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';

/*
  Accessable only by admin, from the admin-view.
  Lets the admin edit the user privileges.
*/
class ManageUserView extends LitElement {
  static get properties () {
    return {
      loggedin: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      user: { type: Object },
      userid: { type: String }
    };
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }


    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 320px 320px 320px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 320px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }

    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 15px 30px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    .infobox {
      padding: 20px 20px 20px 20px ;
      border: 1px solid black;
      background-color: none;
    }

    .label {
      margin-left: 20px;
    }

    `;
  }

  constructor() {
    super();
    this.user = {};
    this.userid = '';
    var url_string = document.URL;
    var url = new URL(url_string);
    this.userid = url.searchParams.get("id");
    this.getuser();

    const data = store.getState();
    this.loggedin = data.user;
    store.subscribe((state)=>{
      this.loggedin = store.getState().loggedin;
    })
  }

  render() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
      </style>

    <a href="${MyAppGlobals.rootPath}adminview" @click="${this.minimize}">Tilbake</a>
    <h2>Rediger bruker</h2>
      <div class="user"><br>
      <style>
        div.avatar {
          background-image: ${this.user.avatar};
          background-size: contain;
          border-radius: 35px;
          background-repeat: no-repeat;
          background-position: center;
        }
      </style>
      <div class="user${this.user.avatar?
        " avatar":""}">
        ${this.user.avatar?"":html`<iron-icon style="--iron-icon-width: 300; --iron-icon-height: 280; align: left;" icon="my-icons:person"></iron-icon>`}

        <div class="infobox">
          <label class="label" for="uid">Bruker ID: ${this.user.id}</label><br><br>
          <label class="label" for="uname">Brukernavn: ${this.user.uname}</label><br><br>
          <label class="label" for="utype">Brukertype: ${this.user.type}</label><br><br><br><br><br>
          <button class="button" @click="${this.makeadmin}" style="float: center">Gjør til administrator</button>
          <button class="button" @click="${this.approveteacher}" style="float: left">Godkjenn lærer</button>
          <button class="button" @click="${this.deleteuser}" style="float: right">Slett bruker</button>
          <button class="button" @click="${this.removepriv}" style="float: right">Fjern tilganger</button>
        </div>
      </div>
    `;
  }

  getuser() {
    const data = new FormData();
    data.append('id', this.userid);
    fetch (`${window.MyAppGlobals.serverURL}api/getsingleuser.php`, {
      method: 'POST',
      credentials: 'include',
      body: data
    }).then(res=>res.json())
    .then(data=>{
      this.user = data;
    });
  }

  approveteacher() {
    console.log("Approve.");
    const data = new FormData();
    data.append('id', this.userid);
    fetch(`${window.MyAppGlobals.serverURL}api/approveteacher.php`, {
        method: 'POST',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      const data1 = new FormData();
      data1.append('id', this.userid);
      fetch (`${window.MyAppGlobals.serverURL}api/getsingleuser.php`, {
        method: 'POST',
        credentials: 'include',
        body: data1
      }).then(res=>res.json())
      .then(data=>{
        this.user = data;
      });
    });
  }

  makeadmin() {
    const data = new FormData();
    data.append('id', this.userid);
    fetch(`${window.MyAppGlobals.serverURL}api/makeuseradmin.php`, {
        method: 'POST',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      const data1 = new FormData();
      data1.append('id', this.userid);
      fetch (`${window.MyAppGlobals.serverURL}api/getsingleuser.php`, {
        method: 'POST',
        credentials: 'include',
        body: data1
      }).then(res=>res.json())
      .then(data=>{
        this.user = data;
      });
    });
  }

  removepriv() {
    const data = new FormData();
    data.append('id', this.userid);
    fetch(`${window.MyAppGlobals.serverURL}api/removeprivileges.php`, {
        method: 'POST',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      const data1 = new FormData();
      data1.append('id', this.userid);
      fetch (`${window.MyAppGlobals.serverURL}api/getsingleuser.php`, {
        method: 'POST',
        credentials: 'include',
        body: data1
      }).then(res=>res.json())
      .then(data=>{
        this.user = data;
      });
    });
  }

  deleteuser() {
    const data = new FormData();
    data.append('id', this.userid);
    console.log(this.userid);
    fetch(`${window.MyAppGlobals.serverURL}api/deleteuser.php`, {
        method: 'POST',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
    });
  }
}

customElements.define('manage-user-view', ManageUserView);
