import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';
import '@vaadin/vaadin-button/vaadin-button.js'

/*
  Accessable only by teachers, from the homepage, videos-view.
  Lets the teacher edit the video, its thumbnail and information.
*/
class ManageVideosView extends LitElement {
  static get properties () {
    return {
      videos: {
        type: Array
      },
      user: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      }
    }
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }

    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 320px 320px 320px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 320px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }

    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    `;
  }

  constructor () {
    super();
    this.videos = [];
    fetch (`${window.MyAppGlobals.serverURL}api/myvideos.php`, {
      credentials: 'include'
    }
    ).then(res=>res.json())
    .then(data=>{
      this.videos = data;
    });
    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
      console.log(this.user);
    });
  }

  render() {
    return html`
    <h1>Administrer videoer</h1>
    <div class="grid-outer">
    <vaadin-button @click="${this.showUpload}">Last opp ny video</vaadin-button>
    <div id="uploadform"  style="visibility: hidden; height: 0px">
    <form id="uploadvideo" method="post" enctype="multipart/form-data">
        <h3>Last opp video</h3>
        <div>
        <label for="title">Tittel</label>
        <input type="text" name="title" required />
      </div>
      <div>
        <label for="description">Beskrivelse</label>
        <input type="text" name="description" required />
      </div>
        <input type="file" name="file" id="file" accept="video/*" required />
        <div class="informative">
          <label for="informative" style="color: red;">.mp4/.ogg/.webm</label>
        </div>
        <button @click="${this.sendFile}">Last opp</button>
        <label id="informUpload" style="visibility: hidden;">Opplasting vellykket</label>
        <progress value="0" max="100" name="progress"></progress>
    </form>
    </div>
      <div class="grid-inner">
      ${this.videos.map(video => {
        return html`
        <div id="video" class="video"">
          <form onsubmit="javascript: return false;">
            <input type="hidden" name="videoid" value="${video.id}" />
            <input type="hidden" name="thumbnail" value="${video.thumbnail}" />
            <input type="hidden" name="filePath" value="${video.filePath}" />
            <input type="hidden" name="subtitles" value="${video.subtitles}" />
            <button @click="${this.deletevideo}" style="float: right">X</button>
          </form>
          ${video.thumbnail?
            html`
            <video width="320" height="240" poster="${video.thumbnail}">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `:
            html`
            <video width="320" height="240">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `}
          <form onsubmit="javascript: return false;">
              Tittel:<input for="title" value="${video.title}"> <br>
              Beskrivelse:<input for="description" value="${video.description}"> <br>
              <div>
                <button @click="${this.updateVideo}">Lagre endringer</button>
                <a id="saved${video.id}" style="visibility: hidden; color: green;">Endringer lagret</a>
              </div>
          </form><br>
          <form onsubmit="javascript: return false;">
            Last opp teksting(.VTT):
            <input type="hidden" name="videoid" value="${video.id}">
            <input type="file" name="file" id="file" accept=".vtt" required />
            <button @click="${this.uploadSubtitles}">Last opp</button>
          </form><br>
          <label for="fileName">Filnavn: ${video.fileName} <br> </label>
          <label for="score">Rating: ${video.avgRating} <br> </label>
          <label for="owner">Lastet opp av: ${video.owner} <br> </label>
          <label for="filePath">Fillokasjon: ${video.filePath} <br> </label>
          ${video.subtitles?
            html`
            <label for="subtitles">Undertekst lokasjon: ${video.subtitles} <br> </label>
            `:html``
          }
          <a href="${MyAppGlobals.rootPath}uploadthumbnail?id=${video.id}">Velg thumbnail</a>
        </div>
        `;
      })}
      </div>
    </div>
    `;
  }

  uploadSubtitles(e) {
    e.preventDefault();
    const data = new FormData(e.target.form);
    fetch(`${window.MyAppGlobals.serverURL}api/uploadsubtitles.php`, {
        method: 'POST',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      fetch (`${window.MyAppGlobals.serverURL}api/myvideos.php`, {
        credentials: 'include'
      }
      ).then(res=>res.json())
      .then(data1=>{
        this.videos = data1;
      });
    });
  }

  showUpload(e) { //toggle or untoggle form for video upload
    var form = this.shadowRoot.getElementById('uploadform'); //Get the form element
    if(form.style.visibility == "hidden") { //If hidden, set to visible
      form.style.visibility="visible";
      form.style.height="auto";
    } else {  //If visible, set to hidden
      form.style.visibility="hidden";
      form.style.height="0px";
    }
  }

  deletevideo(e) {
    const data = new FormData(e.target.form);
    fetch(`${window.MyAppGlobals.serverURL}api/deletevideo.php`, {
        method: 'POST',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      fetch (`${window.MyAppGlobals.serverURL}api/myvideos.php`, {
        credentials: 'include'
      }
      ).then(res=>res.json())
      .then(data1=>{
        this.videos = data1;
      });
    });
  }

  updateVideo(e) {
    console.log(e.target.form);

    const data = new FormData(e.target.form);
    console.log(data);
    /*const sleep = (milliseconds) => {
      return new Promise(resolve => setTimeout(resolve, milliseconds))
    }
    var info = this.shadowRoot.getElementById('saved');
    info.style.visibility="visible";
    sleep(2000).then(() => {
      info.style.visibility="hidden";
    })
    */
  }

  updated(changedProperties) {

      changedProperties.forEach((oldValue, propName) => {

        console.log(`${propName} changed. oldValue: ${oldValue}`);

      });

    }


sendFile(e) {
  e.preventDefault();
  console.log(e.target.form.title.value);
  const file = e.target.form.file.files[0];
  var infoUpload = this.shadowRoot.getElementById("informUpload");
  const progress = e.target.form.querySelector('progress');
  progress.style.display = 'block';

  const xhr = new XMLHttpRequest();
  xhr.file = file; // not necessary if you create scopes like this
  xhr.addEventListener('progress', function(e) {
    const done = e.position || e.loaded, total = e.totalSize || e.total;
    progress.value = (Math.floor(done/total*1000)/10);
    console.log('xhr progress: ' + (Math.floor(done/total*100)) + '%');
  }, false);
  if ( xhr.upload ) {
    xhr.upload.onprogress = function(e) {
      const done = e.position || e.loaded, total = e.totalSize || e.total;
      progress.value = (Math.floor(done/total*100));
      console.log('xhr.upload progress: ' + done + ' / ' + total + ' = ' + (Math.floor(done/total*1000)/10) + '%');
    };
  }
  const _this = this;
  var lock = false;
  xhr.onreadystatechange = function(e) {
    if ( 4 == this.readyState && lock == false) {
      lock = true;
      progress.style.display = 'none';
      const res = JSON.parse(e.target.response);
      console.log(['xhr upload complete', e, res]);
      // File transfer successfull

      fetch (`${window.MyAppGlobals.serverURL}api/myvideos.php`, {
        credentials: 'include'
      }
      ).then(res=>res.json())
      .then(data=>{
        _this.videos = data;
      });
    }
  };
  xhr.withCredentials = true;    // Needed to send the session id cookie
  xhr.open('post', `${window.MyAppGlobals.serverURL}api/uploadProgress.php`, true);
  xhr.setRequestHeader("Content-Type", "application/octet-stream");
  xhr.setRequestHeader("X-TITLE", e.target.form.title.value);
  xhr.setRequestHeader("X-DESCRIPTION", e.target.form.description.value);
  xhr.send(file);
  }

}

customElements.define('manage-videos-view', ManageVideosView);
