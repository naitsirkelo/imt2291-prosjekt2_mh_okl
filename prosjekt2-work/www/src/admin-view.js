import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';


// Accessable only by admins, by the homepage, videos-view.
class AdminView extends LitElement {
  static get properties () {
    return {
      user: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      users: {
        type: Array
      }
    }
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }

    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 220px 220px 220px 220px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 120px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    `;
  }

  /*
    Defining empty users array before fetching users by php script.
    Then loading the correct avatars.
  */
  constructor () {
    super();
    this.users = [];

    fetch (`${window.MyAppGlobals.serverURL}api/getusers.php`)
    .then(res=>res.json())
    .then(data=>{
      this.users = data;
    });
    this.loadAvatars();

    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
    });
  }

  render() {
    return html`
    <h1>Administrer brukere</h1>
    <div class="grid-outer">
      <div class="grid-inner">
      <!-- Showing each user in the map with their own avatar, username. -->
      ${this.users.map(user => {
        return html`
        <div id="user" class="user">
        <style>
          div.avatar {
            background-image: ${user.avatar};
            background-size: contain;
            border-radius: 35px;
            background-repeat: no-repeat;
            background-position: center;
          }
        </style>
        <div class="user${user.avatar?
          " avatar":""}">
          ${user.avatar?"":html`<iron-icon style="--iron-icon-width: 300; --iron-icon-height: 280; align: left;" icon="my-icons:person"></iron-icon>`}
          <label for="uname">${user.uname} (ID: ${user.id})<br></label>
          <label for="uname">${user.type}<br></label>
          <!-- Opening view to manage user, with user id. -->
          <a href="${MyAppGlobals.rootPath}manageuserview?id=${user.id}">Rediger</a>
        </div>
        `;
      })}
      </div>
    </div>
    `;
  }

  /*
    Called when loading the pge, to show the user avatars.
   */
  loadAvatars() {
        this.users.map(user => {
          const data = new FormData();
          data.append("user", user.id);
          fetch (`${window.MyAppGlobals.serverURL}api/avatar.php`, {
            method: 'POST',
            body: data
          })
          .then(res=>{
            this.avatar = res;
            console.log(res);
            fetch (`${window.MyAppGlobals.serverURL}api/getusers.php`)
            .then(res=>res.json())
            .then(data=>{
              this.users = data;
            });
          })
        });
  }

}

customElements.define('admin-view', AdminView);
