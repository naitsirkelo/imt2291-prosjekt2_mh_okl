import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';

/*
  Accessable by all users, logged in or not, from the homepage.
  Lets user watch the chosen video with subtitles and a playback speed option.
*/
class SubtitlesView extends LitElement {
  static get properties () {
    return {
      user: { type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      videoid: { type: String },
      video: { type: Object },
      cues: { type: String },
      activecues: { type: Array }
    };
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    `;
  }

  /*
    Defining empty video object, the subtitle cues and actives as empty arrays.
  */
  constructor () {
    super();
    this.video = {};
    this.cues = '[]';
    this.activecues ='[]';

    this.getvideo();

    this.addEventListener('cuesUpdated', e=> this.cues = JSON.stringify(e.detail.cues));
    this.addEventListener('cuechange', e=> this.activecues = JSON.stringify(e.detail.activeCues));
    this.addEventListener('jumpToTimecode', e=> this.shadowRoot.querySelector('video-viewer').setTime(e.detail.timeCode));

    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
    })
  }

  render() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
      </style>
    <!-- Button to return to the homepage, videos-view. -->
    <a href="${MyAppGlobals.rootPath}videosview">Tilbake</a>
      <div class="video">
        <h2>${this.video.title}</h2>

        <video controls id="video" width="50%" height="auto">
          <source type="video/mp4">
          <track kind="subtitles" label="Undertekster" src="${this.video.subtitles}" srclang="en" default>
        </video><br>
        <div>
        Speed:
          <button id="speed" value="0.5" @click="${this.setPlaybackSpeed}">0.5x</button>
          <button id="speed" value="1.0" @click="${this.setPlaybackSpeed}">1.0x</button>
          <button id="speed" value="2.0" @click="${this.setPlaybackSpeed}">2.0x</button>
        </div>

        <label for="description">Beskrivelse: ${this.video.description} <br> </label>
        <label for="score">Rating: ${this.video.avgRating} <br> </label>
        <label for="owner">Lastet opp av: ${this.video.owner} <br> </label>
        ${this.user.isStudent?
          // Knapp for å gi vurdering, kun for studenter.
          html`
            <label for="filePath">Gi rating: ${this.video.filePath} </label>
          `:
          html``
        }
      </div>
    `;
  }

  /*
    Getting current video ID, and accessing database to get the video source and information.
  */
  getvideo() {
    var url_string = document.URL;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    this.videoid = id;
    const data1 = new FormData();
    data1.append('id', this.videoid);
    fetch (`${window.MyAppGlobals.serverURL}api/getsinglevideo.php`, {
      method: 'POST',
      credentials: 'include',
      body: data1
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      this.video = data;
    }).then(data=>{
      this.shadowRoot.getElementById("video").src=this.video.filePath;
    });
  }

  /*
    Getting the path target e, and setting its shadow root element playbackRate.
  */
  setPlaybackSpeed(e) {
    var value = e.target.value;
    var vid = this.shadowRoot.getElementById('video').playbackRate = value;
  }
}

customElements.define('subtitles-view', SubtitlesView);
