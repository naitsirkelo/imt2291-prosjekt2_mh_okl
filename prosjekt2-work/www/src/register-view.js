import { LitElement, html, css } from 'lit-element';
import '@polymer/iron-icon/iron-icon.js';

/**
 * Shows user symbol to access log in/log out/change user details pane.
 * Works best if put in the upper right corner as a drop down is used to show
 * login/user details screen. If user has an avatar this avatar is shown Instead
 * of the dummy user icon when logged in.
 *
 * @extends LitElement
 */
class RegisterView extends LitElement {
  static get properties () {
    return {
      msg: { type: String }
    }
  }

  /**
   * Get styles for this component.
   *
   * @type {CSS}
   */
  static get styles() {
    return css`
    :host {
      display: block;
      height: 100%;
    }

    iron-icon {
      height: 60px;
      width: 50px;
      margin: 0 5px;
    }

    .status {
      float: center
      top: 64px;
      right: 0px;
      height: 230px;
      width: 380px;
      padding: 5px 10px;
      background-color: none;
    }

    label {
      display: inline-block;
      width: 12em;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    .status>div:first-of-type {
      margin-top: 20px;
    }

    .inputfield {
      display: inline-block;
      width: 350px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid black;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }
    `;
  }

  constructor() {
    super();
    this.msg = '';
  }

  /**
   * Returns the html to render
   *
   * @return {Object} an lit-html object to be rendered.
   */
   render() {
     return html`
      <div class="status">
         <form onsubmit="javascript: return false;">
         <label for="uname">Brukernavn</label><input class="inputfield" type="text" id="uname" name="uname"><br/>
         <div>
           <label for="pwd">Passord</label><input class="inputfield" type="password" id="pwd" name="pwd"><br/>
           <label for="pwd2">Gjenta passord</label><input class="inputfield" type="password" id="pwd2" name="pwd2"><br/>
           <button class="button" @click="${this.register}">Registrer ny bruker</button>
         </div>
         </form><br><br>
         <label for="msg">${this.msg}</label>
       </div>
     `;
   }

  /**
   * Called when the user clicks the register button
   *
   * @param  {Object} e event object from the click on the button. Contains
   * information about the form, the new users iformation.
   */
  register(e) {
    const data = new FormData(e.target.form); // Wrap the form in a FormData object
    fetch (`${window.MyAppGlobals.serverURL}api/register.php`, {
      method: 'POST',
      credentials: "include",
      body: data
    }).then(res=>res.json())
    .then(data=>{
      this.msg = data['msg'];
      console.log(data['msg'])
    });
  }
}

customElements.define('register-view', RegisterView);
