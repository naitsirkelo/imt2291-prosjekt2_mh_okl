/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import './my-icons.js';
import './user-status.js';
import './register-view.js';
import store from './js/store/index';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          --app-primary-color: #425;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }

        .user {
          float: right;
          width: 64px;
          height: 100%;
          border: 1px solid red;
        }

        .menucategory {
          color: var(--app-secondary-color);

        }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>Menu</app-toolbar>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <a name="videosview" href="[[rootPath]]videosview">Videoer</a>
            <a name="playlistsview" href="[[rootPath]]playlistsview">Spillelister</a>
            <a name="registerview" href="[[rootPath]]registerview">Registrer ny bruker</a>
            <template is="dom-if" if="{{user.isTeacher}}">
              <!-- Only teachers will see this. -->
              <app-toolbar>__________________</app-toolbar>
              <a name="managevideos" href="[[rootPath]]managevideos">Administrer dine videoer</a>
              <a name="manageplaylists" href="[[rootPath]]manageplaylists">Administrer dine spillelister</a>
            </template>
            <template is="dom-if" if="{{user.isAdmin}}">
              <!-- Only admins will see this. -->
              <app-toolbar>__________________</app-toolbar>
              <a name="adminview" href="[[rootPath]]adminview">Administrer brukertilganger</a>
            </template>

          </iron-selector>
        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="my-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title="">Student Tube</div>
              <user-status></user-status>
            </app-toolbar>
          </app-header>

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <videos-view name="videosview"></videos-view>
            <playlists-view name="playlistsview"></playlists-view>
            <manage-playlists-view name="manageplaylists"></manage-playlists-view>
            <manage-videos-view name="managevideos"></manage-videos-view>
            <my-view404 name="view404"></my-view404>
            <new-video-view name="newvideo"></new-video-view>
            <upload-thumbnail-view name="uploadthumbnail"></upload-thumbnail-view>
            <subtitles-view name="subtitlesview"></subtitles-view>
            <admin-view name="adminview"></admin-view>
            <manage-user-view name="manageuserview"></manage-user-view>
            <playlist-view name="playlistview"></playlist-view>
            <register-view name="registerview"></register-view>
            <manage-videos-in-playlist-view name="managevideosinplaylistview"></manage-videos-in-playlist-view>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      user: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      }
    };
  }

  constructor() {
    super();
    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
      console.log(this.user);
    })
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'videosview';
    } else if (['videosview', 'playlistsview', 'managevideos','manageplaylists','newvideo','uploadthumbnail','subtitlesview','adminview','manageuserview','playlistview','registerview','managevideosinplaylistview'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    console.log(page);
    switch (page) {
      case 'videosview':
        import('./videos-view.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
      case 'managevideos':
        import('./manage-videos-view.js');
        break;
      case 'manageplaylists':
        import('./manage-playlists-view.js');
        break;
      case 'playlistsview':
        import('./playlists-view.js');
        break;
      case 'newvideo':
        import('./new-video-view.js');
        break;
      case 'uploadthumbnail':
        import('./upload-thumbnail-view.js');
        break;
      case 'subtitlesview':
        import('./subtitles-view.js');
        break;
      case 'adminview':
        import('./admin-view.js');
        break;
      case 'manageuserview':
        import('./manage-user-view.js');
        break;
      case 'playlistview':
        import('./playlist-view.js');
        break;
      case 'registerview':
        import('./register-view.js');
        break;
      case 'managevideosinplaylistview':
        import('./manage-videos-in-playlist-view.js');
        break;
      default:
        break;
    }
  }
}

window.customElements.define('my-app', MyApp);
