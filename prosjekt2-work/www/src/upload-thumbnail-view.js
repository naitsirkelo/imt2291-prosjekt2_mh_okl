import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';
import '@vaadin/vaadin-button/vaadin-button.js'

/*
  Accessable only by teachers, from manage-videos-view.
  Lets the teacher upload a custom thumbnail or choose a thumbnail
  by taking a snapshot in the video.
*/
class UploadThumbnailView extends LitElement {

  static get properties () {
    return {
      user: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      videoid: {
        type: String,
      },
      video: {
        type: Object
      }
    };
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .video {
      margin: 0 auto;
      text-align: center;
    }



    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    `;
  }

  /*
    Defining empty video object, updating stored user info.
  */
  constructor () {
    super();
    this.video = {};
    const data = store.getState();
    this.user = data.user;

    store.subscribe((state)=>{
      this.user = store.getState().user;
      console.log(this.user);
    })
  }

  render() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
    <!-- Button to return to video overview. -->
    <a href="${MyAppGlobals.rootPath}managevideos" @click="${this.minimize}">Tilbake</a>
    <h1>Velg thumbnail</h1>
    <vaadin-button @click="${this.expandUpload}">Last opp fra datamaskin</vaadin-button>
    <vaadin-button @click="${this.expandScreenshot}">Ta screenshot av video</vaadin-button>
      <div id="screenshotdiv" class="video" style="visibility: hidden; height: 0px;">
          <video id="video" width="50%" controls>
            <source type="video/mp4">
            <track kind="subtitles" label="Undertekster" src="${this.video.subtitles}" srclang="en" default>
          </video>
          <canvas id="canvas"></canvas><br>
          <button id="snap" @click="${this.takeSnapshot}">Ta skjermbilde</button>
          <button id="save" @click="${this.saveThumbnailCanvas}">Lagre</button>
          <div>
          Speed:
            <button id="speed" value="0.5" @click="${this.setPlaybackSpeed}">0.5x</button>
            <button id="speed" value="1.0" @click="${this.setPlaybackSpeed}">1.0x</button>
            <button id="speed" value="2.0" @click="${this.setPlaybackSpeed}">2.0x</button>
          </div>

        <label for="title"><br>Tittel: ${this.video.title} <br> </label>
        <label for="description">Beskrivelse: ${this.video.description} <br> </label>
        <label for="fileName">Filnavn: ${this.video.fileName} <br> </label>
        <label for="score">Rating: ${this.video.avgRating} <br> </label>
        <label for="owner">Lastet opp av: ${this.video.owner} <br> </label>
        ${this.user.isTeacher?    //Hvis brukeren er lærer
          html`
            <label for="filePath">Fillokasjon: ${this.video.filePath} </label>
          `:
          html``
        }
      </div>
      <!-- Dropdown menu shown when button to upload video is pressed. -->
      <div id="uploaddiv" style="visibility: hidden; height: 0px;">
        <form method="post" type="multipart/form-data">
          <input type="file" name="file" value="file" accept="image/*" required />
        </form>
      </div>

    `;
  }

  saveThumbnailCanvas(e) {
    var canvas = this.shadowRoot.getElementById("canvas");
    var img = canvas.toDataURL("image/jpeg");
    console.log(img);
    const data = new FormData();
    data.append("videoid", this.videoid);
    data.append("img", img);
    fetch (`${window.MyAppGlobals.serverURL}api/uploadthumbnail.php`, {
      method: 'POST',
      credentials: 'include',
      body: data
    }
  ).then(res=>res.json())
  .then(data=>{
    console.log(data);
  });
  }

  minimize() {
    var target = this.shadowRoot.getElementById('uploaddiv');
    var other = this.shadowRoot.getElementById('screenshotdiv');
    target.style.visibility="hidden";
    other.style.visibility="hidden";
  }

  expandUpload() {
    var url_string = document.URL;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    this.videoid = id;
    var target = this.shadowRoot.getElementById('uploaddiv');
    var other = this.shadowRoot.getElementById('screenshotdiv');
    if(target.style.visibility == "hidden") {
      target.style.visibility="visible";
      target.style.height="auto";
      other.style.height="0px";   //Hide the other form
      other.style.visibility="hidden";
    } else {  //If visible, set to hidden
      target.style.visibility="hidden";
      target.style.height="0px";
    }
  }

  expandScreenshot() {
    var url_string = document.URL;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    this.videoid = id;
    const data1 = new FormData();
    data1.append('id', this.videoid);
    fetch (`${window.MyAppGlobals.serverURL}api/getsinglevideo.php`, {
      method: 'POST',
      credentials: 'include',
      body: data1
    }).then(res=>res.json())
    .then(data=>{
      console.log(data);
      this.video = data;
    }).then(data=>{
      this.shadowRoot.getElementById("video").src=this.video.filePath;
    });

    var target = this.shadowRoot.getElementById('screenshotdiv');
    var other = this.shadowRoot.getElementById('uploaddiv');
    if(target.style.visibility == "hidden") {
      target.style.visibility="visible";
      target.style.height="auto";
      other.style.height="0px";   //Hide the other form
      other.style.visibility="hidden";
    } else {  //If visible, set to hidden
      target.style.visibility="hidden";
      target.style.height="0px";
    }
  }

  takeSnapshot() {
    var video = this.shadowRoot.getElementById('video');
    var canvas = this.shadowRoot.getElementById("canvas");
    var context = canvas.getContext('2d');
    var ratio = video.videoWidth / video.videoHeight;
    var w = video.videoWidth - 10;
    var h = parseInt(w / ratio, 10);
    canvas.width = w;
    canvas.height = h;
    context.fillRect(0, 0, w, h);
    context.drawImage(video, 0, 0, w, h);
  }

  setPlaybackSpeed(e) {
    var value = e.target.value;
    var vid = this.shadowRoot.getElementById('video').playbackRate = value;
  }

}

customElements.define('upload-thumbnail-view', UploadThumbnailView);
