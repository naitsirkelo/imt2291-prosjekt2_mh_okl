import { LitElement, html, css } from 'lit-element';
import store from './js/store/index';

/*
  Accessable by all users, logged in or not, from the homepage.
  Lets user watch any video, but only logged in users can rate the videos.
*/
class VideosView extends LitElement {
  static get properties () {
    return {
      videos: { type: Array },
      playlists: { type: Array },
      user: {
        type: Object,
        value: { student: false, teacher: false, admin: false }
      },
      criteria: { type: String }
    };
  }

  static get styles() {
    return css`
    :host {
      display: block;
    }

    ul {
      list-style-type: none;
    }

    ul li span:first-of-type {
      display: inline-block;
      width: 23em;
    }

    ul li span:nth-of-type(2) {
      display: inline-block;
      width: 14em;
    }

    .grid-inner {
      display: grid;
      grid-template-columns: 320px 320px 320px 320px;
      padding: 10px;
      grid-column-gap: 15px;
    }

    .grid-outer {
      display: block;
      max-width: 1320px;
      margin: 0 auto;
      position: relative;
      height: 100%;
    }

    @media screen and (max-width: 1640px) {
      .grid-inner{
        grid-template-columns: 320px 320px 320px;
      }
      .grid-outer{
        max-width: 1000px;
      }
    }

    @media screen and (max-width: 1320px) {
      .grid-inner{
        grid-template-columns: 320px 320px;
      }
      .grid-outer{
        max-width: 680px;
      }
    }

    @media screen and (max-width: 1000px) {
      .grid-inner{
        grid-template-columns: 320px;
      }
      .grid-outer{
        max-width: 360px;
      }
    }

    .inputfield {
      width: 320px;
      padding: 10px 10px;
      margin: 1px 0;
      box-sizing: border-box;
      border: 1px solid #555;
      outline: none;
    }

    .inputfield:focus {
      background-color: lightblue;
    }

    .button {
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 1px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      background-color: white;
      color: black;
      border: 1px solid black;
    }

    .button:hover {
      background-color: #008CBA;
      color: white;
    }

    `;
  }

  /*
    Defining empty videos array and criteria string, then loading videos into array by php.
  */
  constructor () {
    super();
    this.videos = [];
    this.playlists = [];
    this.criteria = '';

    // Getting all videos and subscribed playlists, if there are any.
    this.getvideos();

    const data = store.getState();
    this.user = data.user;
    store.subscribe((state)=>{
      this.user = store.getState().user;
      console.log(this.user);
    });

    this.getplaylists();
  }

  render() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

    <div class="card">
      <form onsubmit="javascript: return false;">
        <label>
          <h3>Søk etter video</h3>
          <input class="inputfield" type="text" name="criteria" placeholder="Søk etter tittel, foreleser, beskrivelse etc...">
          <button class="button" @click="${this.search}">Søk</button>
        </label>
      </form>
    </div>
    ${this.user.isStudent?    //Hvis brukeren er student
      html`
      <h1>Abonnementer</h1>
      <div class="grid-outer">
        <div class="grid-inner">
        <!-- Showing each playilst the student is subscribed to. -->
        ${this.playlists.map(playlist => {
          return html`
          <div class="playlist">
            <img src="${playlist.thumbnail}" height="240" width="320">
            <label for="title"><br>Tittel: ${playlist.title} <br> </label>
            <label for="description">Beskrivelse: ${playlist.description} <br> </label>
            <label for="subject">Emne: ${playlist.subject} <br> </label>
            <label for="theme">Tema: ${playlist.theme} <br> </label>
            <label for="owner">Lastet opp av: ${playlist.owner} <br> </label>
            <!-- Link to open view to watch chosen video with subtitles. -->
            <a href="${MyAppGlobals.rootPath}playlistview?id=${playlist.id}">Se videoer</a>
          </div>
          `;
        })}
        </div>
      </div>
      `:
      html``
    }
    <h1>Videoer</h1>
    <div class="grid-outer">
      <div class="grid-inner">
      <!-- Showing each video in the map with their own thumbnail, controls, etc. -->
      ${this.videos.map(video => {
        return html`
        <div class="video">
          ${video.thumbnail?
            html`
            <video controls width="320" height="240" poster="${video.thumbnail}">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `:
            html`
            <video controls width="320" height="240">
              <source src="${video.filePath}" type="video/mp4">
            </video>
            `}
          <!-- Link to open view to watch chosen video with subtitles. -->
          <a href="${MyAppGlobals.rootPath}subtitlesview?id=${video.id}">Se med undertekster</a>
          <label for="title"><br>Tittel: ${video.title} <br> </label>
          <label for="description">Beskrivelse: ${video.description} <br> </label>
          <label for="fileName">Filnavn: ${video.fileName} <br> </label>
          <label for="score">Rating: ${video.avgRating} <br> </label>
          <label for="owner">Lastet opp av: ${video.owner} <br> </label>
          ${this.user.isStudent?    //Hvis brukeren er student
            html`
            <label for="rating">Gi rating: <br> </label>
            <form method="post">
              <input type="hidden" name="videoid" value="${video.id}" />
              <button @click="${this.rateVideo}" value="1">1</button>
              <button @click="${this.rateVideo}" value="2">2</button>
              <button @click="${this.rateVideo}" value="3">3</button>
              <button @click="${this.rateVideo}" value="4">4</button>
              <button @click="${this.rateVideo}" value="5">5</button>
            </form>
            `:
            html``
          }
          ${this.user.isTeacher?    //Hvis brukeren er lærer
            html`
              <label for="filePath">Fillokasjon: ${video.filePath} </label>
            `:
            html``
          }
        </div>
        `;
      })}
      </div>
    </div>
    `;
  }

  /**
   * Called from constructor, loads all videos into video array.
   */
  getvideos() {
    fetch (`${window.MyAppGlobals.serverURL}api/videos.php`)
    .then(res=>res.json())
    .then(data=>{
      this.videos = data;
    });
  }

  /**
   * Called from constructor, loads all subscribed playlists to the top of the
   homepage view, if the user is a student.
   */
  getplaylists() {
    const data = new FormData();
    data.append('uid', this.user.uid);
    fetch (`${window.MyAppGlobals.serverURL}api/subscribedplaylists.php`, {
        method: 'POST',
        credentials: 'include',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      this.playlists = data;
    });
  }

  /**
   * Called when the user clicks the rating manager below the video.
   *
   * @param  {Object} e event object that contains information about the video
   */
  rateVideo(e) {
    e.preventDefault();
    var rating = e.target.value;
    var videoid = e.target.form.videoid.value;

    const data = new FormData();
    data.append("videoid", videoid);
    data.append("rating", rating);
    fetch(`${window.MyAppGlobals.serverURL}api/ratevideo.php`, {
        method: 'POST',
        credentials: 'include',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      fetch (`${window.MyAppGlobals.serverURL}api/videos.php`)
      .then(res=>res.json())
      .then(data=>{
        this.videos = data;
      });
    });
  }

  /**
   * Called when the user clicks the search button after they enter a search criteria.
   *
   * @param  {Object} e event object that contains the search criteria.
   */
  search(e) {
    const data = new FormData(e.target.form);
    fetch(`${window.MyAppGlobals.serverURL}api/searchvideo.php`, {
        method: 'POST',
        credentials: 'include',
        body: data
    }).then(res=>res.json())
    .then(data=>{
      this.videos = data;
    });
  }
}

customElements.define('videos-view', VideosView);
